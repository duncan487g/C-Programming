# combined-template

- For mdbook, reference [here](https://gitlab.com/90cos/public/training/templates/mdbook-template)
- For Reveal JS Slides, reference [here](https://gitlab.com/90cos/public/training/templates/slides-template)

# Add multiple RevealJS SlideDecks

You are able to create multiple slide decks in the same mdbook. These slide decks are logically separated and do not interact with eachother. They can be access by adding a ```/slides``` at the end of your URL (depending on where you place them). There have been multiple slide deck added to this template to help you understand how to implement it yourself.

You will notice that there are separate slides located at different levels of the ```mdbook/src``` folder. They are listed below..
```sh
mdbook/src/cli/slides
mdbook/src/for_developers/slides
mdbook/src/format/slides
mdbook/src/slides
```
Each one of these ```slides``` folders will have an ```index.html``` file defining the slides content. To understand how to change this file to reflect your slide, check out the 'Reveal JS Slides, references' above.

You can place a copy of this ```slides``` folder anywhere within the ```mdbook/src``` folder and the gitlab pipeline will create your slides which can be accessed at the URL representative of the relative path.